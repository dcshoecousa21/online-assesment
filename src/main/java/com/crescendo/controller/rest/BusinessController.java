package com.crescendo.controller.rest;

import com.crescendo.model.Business;
import com.crescendo.service.IBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/business")
public class BusinessController {
    @Autowired
    IBusinessService businessService;

    @GetMapping("/getAllBusiness")
    public List<Business> getAllBusiness(){
        return businessService.getAllBusiness();
    }

    @GetMapping("/getBusinessById")
    public Business getBusinessById(@RequestParam(required = true) UUID id){
        return businessService.getBusinessById(id);
    }

    @PostMapping("/saveNewBusinessEntity")
    public boolean saveNewBusiness (@RequestBody(required = true)Business business){
        return businessService.saveNewBusinessEntity(business);
    }

    @PutMapping("/updateBusinessEntity")
    public boolean updateBusinessEntity (@RequestBody Business business){
        return businessService.updateBusinessEntity(business);
    }

    @DeleteMapping("/deleteBusinessEntity")
    public boolean deleteBusinessEntity (@RequestParam UUID id){
        return businessService.deleteBusinessEntity(id);
    }

}
