package com.crescendo.service;

import com.crescendo.model.Business;
import com.crescendo.repository.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class BusinessServiceImpl implements IBusinessService{

    @Autowired
    private BusinessRepository businessRepository;

    @Override
    public List<Business> getAllBusiness() {
        return businessRepository.findAll();
    }

    @Override
    public Business getBusinessById(UUID id) {
        Business businessRes = businessRepository.getById(id);
        return businessRes;
    }

    @Override
    public boolean saveNewBusinessEntity(Business businessObj) {
        if (businessObj == null)
        {
            return false;
        }else
        {
            businessRepository.save(businessObj);
            return true;
        }
    }

    @Override
    public boolean updateBusinessEntity(Business businessObj) {
        if (businessObj == null)
        {
            return false;
        }else
        {
            businessRepository.saveAndFlush(businessObj);
            return true;
        }
    }

    @Override
    public boolean deleteBusinessEntity(UUID id) {
        if (id == null)
        {
            return false;
        }else
        {
            businessRepository.deleteById(id);
            return true;
        }
    }
}
