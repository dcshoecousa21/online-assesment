package com.crescendo.service;

import com.crescendo.model.Review;
import com.crescendo.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ReviewServiceImpl implements IReviewService{

    @Autowired
    ReviewRepository reviewRepository;

    @Override
    public List<Review> getAllReviews() {
        return reviewRepository.findAll();
    }

    @Override
    public boolean saveReview(Review reviewObj) {
        if (reviewObj.getId().equals(null)){
            return false;
        }else {
            reviewRepository.save(reviewObj);
            return true;
        }
    }
}
