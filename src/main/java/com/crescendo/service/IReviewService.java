package com.crescendo.service;

import com.crescendo.model.Review;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IReviewService {
    public List<Review> getAllReviews();

    public boolean saveReview(Review reviewObj);
}
