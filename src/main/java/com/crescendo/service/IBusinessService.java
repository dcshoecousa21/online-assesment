package com.crescendo.service;

import com.crescendo.model.Business;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public interface IBusinessService {

    public List<Business> getAllBusiness();

    public Business getBusinessById(UUID id);

    public boolean saveNewBusinessEntity(Business businessObj);

    public boolean updateBusinessEntity(Business businessObj);

    public boolean deleteBusinessEntity(UUID id);
}
